//
//  CameraScannerConfigurator.swift
//  PokemonScanner
//
//  Created by admin on 12/11/19.
//  Copyright (c) 2019 PNsoi. All rights reserved.
//

import UIKit

class CameraScannerConfigurator {
    
    static let shared = CameraScannerConfigurator()
    
    func configure(viewController: CameraScannerViewController) {
        let interactor = CameraScannerInteractor()
        let presenter = CameraScannerPresenter()
        let router = CameraScannerRouter()
        
        viewController.interactor = interactor
        viewController.router = router
        
        interactor.presenter = presenter
        presenter.viewController = viewController
        
        router.viewController = viewController
        router.dataStore = interactor
    }
}
