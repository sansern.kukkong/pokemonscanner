//
//  CameraScannerViewController.swift
//  PokemonScanner
//
//  Created by admin on 12/11/19.
//  Copyright (c) 2019 PNsoi. All rights reserved.
//

import UIKit

class CameraScannerViewController: BaseViewController {
    
    // MARK: Property
    
    var interactor: CameraScannerBusinessLogic?
    var router: (NSObjectProtocol & CameraScannerRoutingLogic & CameraScannerDataPassing)?
    
    // MARK: IBOutlet Property
    
    

    // MARK: Object lifecycle
  
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        CameraScannerConfigurator.shared.configure(viewController: self)
    }
  
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        CameraScannerConfigurator.shared.configure(viewController: self)
    }
  
    // MARK: View lifecycle
  
    override func viewDidLoad() {
        super.viewDidLoad()
        doSomething()
    }
  
    // MARK: Request
  
    func doSomething() {
        let request = CameraScannerScene.Something.Request()
        interactor?.doSomething(request: request)
    }
    
    // MARK: Methods
    
}

protocol CameraScannerDisplayLogic: class {
    func displaySomething(viewModel: CameraScannerScene.Something.ViewModel)
}

extension CameraScannerViewController: CameraScannerDisplayLogic {
    
    func displaySomething(viewModel: CameraScannerScene.Something.ViewModel) {
        
    }
    
}
