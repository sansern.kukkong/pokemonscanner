//
//  CameraScannerRouter.swift
//  PokemonScanner
//
//  Created by admin on 12/11/19.
//  Copyright (c) 2019 PNsoi. All rights reserved.
//

import UIKit

@objc protocol CameraScannerRoutingLogic {
//    func routeToSomewhere()
}

protocol CameraScannerDataPassing {
    var dataStore: CameraScannerDataStore? { get }
}

class CameraScannerRouter: NSObject, CameraScannerRoutingLogic, CameraScannerDataPassing {
    weak var viewController: CameraScannerViewController?
    var dataStore: CameraScannerDataStore?
  
    // MARK: Routing
  
//    func routeToSomewhere() {
//        let destinationVC = SomewhereViewController(nibName: "Somewhere", bundle: nil)
//        var destinationDS = destinationVC.router!.dataStore!
//        passDataToSomewhere(source: dataStore!, destination: &destinationDS)
//        navigateToSomewhere(source: viewController!, destination: destinationVC)
//    }

    // MARK: Navigation
  
//    func navigateToSomewhere(source: CameraScannerViewController, destination: SomewhereViewController) {
//        let navigationController = UINavigationController(rootViewController: destination)
//        source.present(navigationController, animated: true, completion: nil)
//    }
  
    // MARK: Passing data
  
//    func passDataToSomewhere(source: CameraScannerDataStore, destination: inout SomewhereDataStore) {
//        destination.name = source.name
//    }
    
}
