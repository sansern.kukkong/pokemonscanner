//
//  CameraScannerPresenter.swift
//  PokemonScanner
//
//  Created by admin on 12/11/19.
//  Copyright (c) 2019 PNsoi. All rights reserved.
//

import UIKit

protocol CameraScannerPresentationLogic {
    func presentSomething(response: CameraScannerScene.Something.Response)
}

class CameraScannerPresenter: CameraScannerPresentationLogic {
    weak var viewController: CameraScannerDisplayLogic?
  
    func presentSomething(response: CameraScannerScene.Something.Response) {
        let viewModel = CameraScannerScene.Something.ViewModel()
        viewController?.displaySomething(viewModel: viewModel)
    }
    
}
