//
//  CameraScannerInteractor.swift
//  PokemonScanner
//
//  Created by admin on 12/11/19.
//  Copyright (c) 2019 PNsoi. All rights reserved.
//

import UIKit

protocol CameraScannerBusinessLogic {
    func doSomething(request: CameraScannerScene.Something.Request)
}

protocol CameraScannerDataStore {
//    var name: String { get set }
}

class CameraScannerInteractor: CameraScannerBusinessLogic, CameraScannerDataStore {
    var presenter: CameraScannerPresentationLogic?
    var worker: CameraScannerWorker?
//    var name: String = ""
  
    // MARK: Do something
  
    func doSomething(request: CameraScannerScene.Something.Request) {
        worker = CameraScannerWorker()
        worker?.doSomeWork()
    
        let response = CameraScannerScene.Something.Response()
        presenter?.presentSomething(response: response)
    }
    
}
