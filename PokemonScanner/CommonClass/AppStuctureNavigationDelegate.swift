//
//  AppStuctureNavigationDelegate.swift
//  AppStructure
//
//  Created by PuPromptnow on 3/12/2562 BE.
//  Copyright © 2562 Pucheeze. All rights reserved.
//

import UIKit

protocol AppStuctureNavigationDelegate {
    func presentNavigationController(
        rootViewController: UIViewController,
        modalPresentationStyle: UIModalPresentationStyle,
        animated: Bool,
        completion: (() -> Void)?
    )
}

extension AppStuctureNavigationDelegate where Self: UIViewController {
    func presentNavigationController(
        rootViewController: UIViewController,
        modalPresentationStyle: UIModalPresentationStyle = .fullScreen,
        animated: Bool = true,
        completion: (() -> Void)? = nil
    ) {
        let navigationController = UINavigationController(rootViewController: rootViewController)
        navigationController.modalPresentationStyle = modalPresentationStyle
        present(navigationController, animated: animated, completion: completion)
    }
}

