//
//  BasedViewController.swift
//  AppStructure
//
//  Created by Pucheeze on 26/10/2561 BE.
//  Copyright © 2561 Pucheeze. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController, AppStuctureNavigationDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
        dismissKeyboard()
    }
    
    func dismissKeyboard() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(_:)))
        view.addGestureRecognizer(tap)
        view.isUserInteractionEnabled = true
    }
    
    @objc private func dismissKeyboard(_ sender: UIGestureRecognizer) {
        view.endEditing(true)
    }

}
