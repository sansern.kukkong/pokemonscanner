//
//  MainMenuPresenter.swift
//  PokemonScanner
//
//  Created by admin on 12/11/19.
//  Copyright (c) 2019 PNsoi. All rights reserved.
//

import UIKit

protocol MainMenuPresentationLogic {
    func presentSomething(response: MainMenuScene.Something.Response)
}

class MainMenuPresenter: MainMenuPresentationLogic {
    weak var viewController: MainMenuDisplayLogic?
  
    func presentSomething(response: MainMenuScene.Something.Response) {
        let viewModel = MainMenuScene.Something.ViewModel()
        viewController?.displaySomething(viewModel: viewModel)
    }
    
}
