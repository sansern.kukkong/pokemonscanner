//
//  MainMenuViewController.swift
//  PokemonScanner
//
//  Created by admin on 12/11/19.
//  Copyright (c) 2019 PNsoi. All rights reserved.
//

import UIKit

class MainMenuViewController: BaseViewController {
    
    // MARK: Property
    
    var interactor: MainMenuBusinessLogic?
    var router: (NSObjectProtocol & MainMenuRoutingLogic & MainMenuDataPassing)?
    
    // MARK: IBOutlet Property
    
    

    // MARK: Object lifecycle
  
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        MainMenuConfigurator.shared.configure(viewController: self)
    }
  
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        MainMenuConfigurator.shared.configure(viewController: self)
    }
  
    // MARK: View lifecycle
  
    override func viewDidLoad() {
        super.viewDidLoad()
        doSomething()
    }
  
    // MARK: Request
  
    func doSomething() {
        let request = MainMenuScene.Something.Request()
        interactor?.doSomething(request: request)
    }
    
    // MARK: Methods
    
    @IBAction func didClickOpenCameraButton() {
        router?.routeToCameraScanner()
    }
    
}

protocol MainMenuDisplayLogic: class {
    func displaySomething(viewModel: MainMenuScene.Something.ViewModel)
}

extension MainMenuViewController: MainMenuDisplayLogic {
    
    func displaySomething(viewModel: MainMenuScene.Something.ViewModel) {
        
    }
    
}
