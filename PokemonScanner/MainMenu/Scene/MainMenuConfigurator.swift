//
//  MainMenuConfigurator.swift
//  PokemonScanner
//
//  Created by admin on 12/11/19.
//  Copyright (c) 2019 PNsoi. All rights reserved.
//

import UIKit

class MainMenuConfigurator {
    
    static let shared = MainMenuConfigurator()
    
    func configure(viewController: MainMenuViewController) {
        let interactor = MainMenuInteractor()
        let presenter = MainMenuPresenter()
        let router = MainMenuRouter()
        
        viewController.interactor = interactor
        viewController.router = router
        
        interactor.presenter = presenter
        presenter.viewController = viewController
        
        router.viewController = viewController
        router.dataStore = interactor
    }
}
