//
//  MainMenuInteractor.swift
//  PokemonScanner
//
//  Created by admin on 12/11/19.
//  Copyright (c) 2019 PNsoi. All rights reserved.
//

import UIKit

protocol MainMenuBusinessLogic {
    func doSomething(request: MainMenuScene.Something.Request)
}

protocol MainMenuDataStore {
//    var name: String { get set }
}

class MainMenuInteractor: MainMenuBusinessLogic, MainMenuDataStore {
    var presenter: MainMenuPresentationLogic?
    var worker: MainMenuWorker?
//    var name: String = ""
  
    // MARK: Do something
  
    func doSomething(request: MainMenuScene.Something.Request) {
        worker = MainMenuWorker()
        worker?.doSomeWork()
    
        let response = MainMenuScene.Something.Response()
        presenter?.presentSomething(response: response)
    }
    
}
