//
//  MainMenuRouter.swift
//  PokemonScanner
//
//  Created by admin on 12/11/19.
//  Copyright (c) 2019 PNsoi. All rights reserved.
//

import UIKit

@objc protocol MainMenuRoutingLogic {
//    func routeToSomewhere()
    func routeToCameraScanner()
}

protocol MainMenuDataPassing {
    var dataStore: MainMenuDataStore? { get }
}

class MainMenuRouter: NSObject, MainMenuRoutingLogic, MainMenuDataPassing {
    weak var viewController: MainMenuViewController?
    var dataStore: MainMenuDataStore?
  
    // MARK: Routing

    func routeToCameraScanner() {
        guard let viewController = viewController else { return }
        guard let dataStore = dataStore else { return }
        let destinationVC = CameraScannerViewController(nibName: "CameraScannerView", bundle: nil)
        guard let destinationRouter = destinationVC.router,
            let destinationDataStore = destinationRouter.dataStore else { return }
        var destinationDS = destinationDataStore
        passDataToCameraScanner(source: dataStore, destonation: &destinationDS)
        navigateToCameraScanner(source: viewController, destination: destinationVC)
    }

    // MARK: Navigation
  
    func navigateToCameraScanner(source: MainMenuViewController, destination: CameraScannerViewController) {
        source.presentNavigationController(rootViewController: destination, animated: true, completion: nil)
    }
    
    // MARK: Passing data
  
    func passDataToCameraScanner(source: MainMenuDataStore, destonation: inout CameraScannerDataStore) {
        
    }
}
